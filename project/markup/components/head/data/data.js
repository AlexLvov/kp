head: {
    defaults: {
        title: 'Index',
        indexPage: true
    },
    team: {
        title: 'Команда',
        indexPage: false
    },
    home: {
        title: 'Главная',
        indexPage: false
    }
}
